// Fill out your copyright notice in the Description page of Project Settings.


#include "SGameInstance.h"

void USGameInstance::Init()
{
	Super::Init();
	
}

void USGameInstance::SetMapSize(int NewMapSize)
{
	MapSize = NewMapSize;
}

void USGameInstance::LoadGameInstance_Implementation(USSaveGame* SaveObject)
{
	if(SaveObject)
	{
		MapSize = SaveObject->MepSize;
	}
}
