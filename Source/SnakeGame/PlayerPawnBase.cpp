// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "WidgetGameOver.h"
#include "Components/InputComponent.h"
#include "Kismet/GameplayStatics.h"


APlayerPawnBase::APlayerPawnBase()
{

	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));

	RootComponent = PawnCamera;
}

void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	
	FName CurrentLevelName = FName(UGameplayStatics::GetCurrentLevelName(GetWorld()));
	ASnakeGameMode* GameMode = GetWorld()->GetAuthGameMode<ASnakeGameMode>();
	
	if (CurrentLevelName != FName("Menu"))
	{
			SetActorRotation(FRotator(-90, 0, 0));
			SetActorLocation(FVector(0, 0, 1600));
			CreateFloorActor();
			CreateSnakeActor();
	}
	
	FInputModeGameOnly InputModeData;
	InputModeData.SetConsumeCaptureMouseDown(true);
	
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	PlayerController->SetInputMode(InputModeData);
}

void APlayerPawnBase::CreateGameOverWidget()
{
	GameOverWidget = CreateWidget<UWidgetGameOver>(GetWorld(),GameOverWidgetClass);
	GameOverWidget->Setup();
}

void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
	
	PlayerInputComponent->BindAction("Restart",IE_Pressed, this, &APlayerPawnBase::RestartGame);
}

void APlayerPawnBase::CreateSnakeActor()
{
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.Instigator = this;
	
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform(), SpawnParameters);
	LostSpeed = SnakeActor->GetMovementSpeed();
}

void APlayerPawnBase::CreateFloorActor()
{
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.Instigator = this;
	
	FloorActor = GetWorld()->SpawnActor<ASFloor>(FloorActorClass, FTransform(), SpawnParameters);
	FloorActor->AddActorWorldOffset(FVector::ZeroVector);
}

void APlayerPawnBase::PlayMoveSound(FVector Location) const
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), MoveSound, Location);
}

void APlayerPawnBase::PlayEatSound(FVector Location) const
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), EatSound, Location);
}

void APlayerPawnBase::PlayDiedSound(FVector Location) const
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), DiedSound, Location);
}

void APlayerPawnBase::PlayPortalSound(FVector Location) const
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), PortalSound, Location);
}

void APlayerPawnBase::RestartGame()
{
	if(IsValid(GameOverWidget) && GameOverWidget->IsVisible())
	{
		GameOverWidget->RestartButtonClicked();
	}
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::DOWN)
		{
			if (SnakeActor->GetIsAbeleMove())
			{
				return;
			}
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
			SnakeActor->SetIsAbeleMove(true);
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP)
		{
			if (SnakeActor->GetIsAbeleMove())
			{
				return;
			}
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
			SnakeActor->SetIsAbeleMove(true);
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
		{
			if (SnakeActor->GetIsAbeleMove())
			{
				return;
			}
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
			SnakeActor->SetIsAbeleMove(true);
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
		{
			if (SnakeActor->GetIsAbeleMove())
			{
				return;
			}
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
			SnakeActor->SetIsAbeleMove(true);
		}
	}
}
