// Copyright Epic Games, Inc. All Rights Reserved.


#include "SnakeGameMode.h"

#include <SnakeGame/SSaveGame.h>
#include <Kismet/GameplayStatics.h>

#include "SGameInstance.h"
#include "SPlayerState.h"
#include "GameFramework/GameStateBase.h"

ASnakeGameMode::ASnakeGameMode()
{
	SlotName = "BestResult";
}

void ASnakeGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

	LoadSaveGame();
}

void ASnakeGameMode::HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer)
{
	Super::HandleStartingNewPlayer_Implementation(NewPlayer);
	
	ASPlayerState* PS = NewPlayer->GetPlayerState<ASPlayerState>();
	//USGameInstance* GI = NewPlayer->GetGameInstance<USGameInstance>();
	if(PS)
	{
		PS->LoadPlayerState(CurrentSaveGame);
		//GI->LoadGameInstance(CurrentSaveGame);
	}
}

void ASnakeGameMode::WriteSaveGame()
{
	ASPlayerState* PS = Cast<ASPlayerState>(GameState->PlayerArray[0]);
	if(PS)
	{
		PS->SavePlayerState(CurrentSaveGame);
		
	}
	
	UGameplayStatics::SaveGameToSlot(CurrentSaveGame, SlotName, 0);
}

void ASnakeGameMode::LoadSaveGame()
{
	if (UGameplayStatics::DoesSaveGameExist(SlotName, 0))
	{
		CurrentSaveGame = Cast<USSaveGame>(UGameplayStatics::LoadGameFromSlot(SlotName, 0));
		if(CurrentSaveGame == nullptr)
		{
			UE_LOG(LogTemp, Warning, TEXT("Filed To Load SaveGame Data."))
			return;
		}
		UE_LOG(LogTemp, Warning, TEXT("Loaded SaveGame Data."))
	}
	else
	{
		CurrentSaveGame = Cast<USSaveGame>(UGameplayStatics::CreateSaveGameObject(USSaveGame::StaticClass()));

		UE_LOG(LogTemp, Warning, TEXT("Create new SaveGame Data."))
	}
}
