// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"

#include "SFloor.h"
#include "SnakeBase.h"
#include "SPlayerState.h"
#include "Kismet/KismetMathLibrary.h"

AFood::AFood()
{
	PrimaryActorTick.bCanEverTick = true;

	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>("MeshComp");
	RootComponent = StaticMeshComp;
	StaticMeshComp->SetRelativeScale3D(FVector(0.3f, 0.3f, 0.3f));
}

void AFood::EatFood(ASnakeBase* SnakeBase, float SpeedBonus, int AddElement)
{
	SnakeBase->AddSnakeElement(AddElement);
	SnakeBase->SetMovementSpeed(SpeedBonus);

	if (const APlayerPawnBase* PlayerPawnBase = SnakeBase->GetInstigator()->GetController()->GetPawn<APlayerPawnBase>())
	{
		PlayerPawnBase->PlayEatSound(GetActorLocation());
	}
	
	if(ASPlayerState* PlayerState = SnakeBase->GetInstigator()->GetPlayerState<ASPlayerState>())
	{
		PlayerState->AddScore(AddElement * 100);
	}
}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	int EatIndex = UKismetMathLibrary::RandomIntegerInRange(0, 1);

	if (bIsHead)
	{				
		if (ASnakeBase* Snake = Cast<ASnakeBase>(Interactor))
		{
			SetActorHiddenInGame(true);
			switch (EatIndex)
			{
			case 0:

				EatFood(Snake, Snake->GetMovementSpeed() - 0.003f, 2);				
				GEngine->AddOnScreenDebugMessage(3, 2, FColor::Cyan,"Eat 2 ElementBonus", true);
				break;

			case 1:
				
				EatFood(Snake, Snake->GetMovementSpeed() + 0.002f, 1);
				GEngine->AddOnScreenDebugMessage(3, 2, FColor::Green,"Eat 1 ElementBonus", true);
				break;
				
			default:;
			}
			
			if (IsValid(FloorRef))
			{
				FloorRef->AddFood();
			}
		}
	}
}
