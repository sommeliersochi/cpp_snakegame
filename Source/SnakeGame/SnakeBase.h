// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MagicNumberTypes.h"
#include "PlayerPawnBase.h"
#include "SnakeGameMode.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()

public:

	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;
	
	UPROPERTY()
	EMovementDirection LastMoveDirection;

	bool InPortal = false;
	
protected:

	virtual void BeginPlay() override;
	
	bool IsMoveProgress = false;

	UPROPERTY()
	ASnakeElementBase* PrevElement = nullptr;
	
	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY(EditDefaultsOnly)
	UMaterialInterface* ElementMaterial;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize = ELEMENT_SIZE;

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed = 0.4f;

	UPROPERTY(EditDefaultsOnly)
	UMaterialInterface* DeadMaterial;

	UPROPERTY()
	APlayerPawnBase* PlayerPawnBase;
 
public:

	virtual void Tick(float DeltaTime) override;

	virtual void Destroyed() override;

	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsNum = 1);

	ASnakeGameMode* GetGameMode();

	UFUNCTION(BLueprintCallable)
	void Move();

	UFUNCTION()
	float SetMovementSpeed(float NewMovementSpeed);	

	UFUNCTION()
	void SetIsAbeleMove(bool IsValue);

	UFUNCTION(BlueprintCallable)
	float GetMovementSpeed() const;

	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

	FORCEINLINE ASnakeElementBase* GetPrevElement() const
	{
		return PrevElement;
	}
	
	FORCEINLINE TArray<ASnakeElementBase*> GetSnakeElements()
	{
		return SnakeElements;
	}

	FORCEINLINE bool GetIsAbeleMove() const
	{
		return IsMoveProgress;
	}
};
