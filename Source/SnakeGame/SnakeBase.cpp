// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"

#include "SnakeElementBase.h"
#include "Interactable.h"
#include "PlayerPawnBase.h"
#include "Math/UnrealMathUtility.h"


ASnakeBase::ASnakeBase()
{
	PrimaryActorTick.bCanEverTick = true;
	
	LastMoveDirection = EMovementDirection::DOWN;
}

void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	PlayerPawnBase = Cast<APlayerPawnBase>(GetInstigatorController()->GetPawn());


	
	AddSnakeElement(3);
	
}

void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	SetActorTickInterval(MovementSpeed);
	MovementSpeed = FMath::Clamp(MovementSpeed, 0.2f, .8f);
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	FVector PrevLocation = FVector::ZeroVector;
	
	for (int i = 0; i < ElementsNum; ++i)
	{
		for (int j = SnakeElements.Num() - 1; j > 0; j--)
		{
			PrevElement = SnakeElements[j];
			PrevLocation = PrevElement->GetActorLocation();	

		}
		
		FVector NewLocation(PrevLocation);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);

		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
		
		if(ElemIndex != 0 && ElemIndex % 2 == 0)
		{
			SnakeElements[ElemIndex]->GetMesh()->SetMaterial(0,ElementMaterial);
			
		}
	}
}

ASnakeGameMode* ASnakeBase::GetGameMode()
{
	ASnakeGameMode* GameMode = GetWorld()->GetAuthGameMode<ASnakeGameMode>();
	
	return GameMode;
}


float ASnakeBase::SetMovementSpeed(float NewMovementSpeed)
{
	return MovementSpeed = NewMovementSpeed;
}

void ASnakeBase::SetIsAbeleMove(bool IsValue)
{
	IsMoveProgress = IsValue;
}

float ASnakeBase::GetMovementSpeed() const
{
	return MovementSpeed;
}

void ASnakeBase::Move()
{
	FVector MovementVector(FVector::ZeroVector);

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	SnakeElements[0]->ToggleColision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		PrevElement = SnakeElements[i - 1];

		auto BaseRot = PrevElement->GetActorRotation();
		//PrevElement->SetActorRotation(FRotator(MovementVector.X,0, 0));
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	
	SetIsAbeleMove(false);
	SnakeElements[0]->AddActorWorldOffset(MovementVector);	
	SnakeElements[0]->ToggleColision();
	PlayerPawnBase->PlayMoveSound(SnakeElements[0]->GetActorLocation());
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::Destroyed()
{
	Super::Destroyed();
	if (PlayerPawnBase)
	{
		PrevElement->GetMesh()->SetMaterial(0,DeadMaterial);
		
		PlayerPawnBase->CreateGameOverWidget();
		PlayerPawnBase->PlayDiedSound(GetActorLocation());
		
	}
}
