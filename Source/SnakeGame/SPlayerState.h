// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "SPlayerState.generated.h"

class USSaveGame;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnScoreChanged, ASPlayerState*, PlayerState, int32, NewScore, int32,
                                               Delta);
                                               
UCLASS()
class SNAKEGAME_API ASPlayerState : public APlayerState
{
	GENERATED_BODY()
	
protected:
	
	UPROPERTY(EditDefaultsOnly, Category= "Score")
	int32 PlayScore = 0;

	int32 BestScore;

public:

	UFUNCTION(BlueprintCallable, Category= "Score")
	int32 GetCresdits() const;

	UFUNCTION(BlueprintCallable, Category= "Score")
	int32 GetBestResult() const;

	UFUNCTION(BlueprintCallable, Category= "Score")
	void AddScore(int32 Delta);

	UPROPERTY(BlueprintAssignable, Category= "Events")
	FOnScoreChanged OnCreditsChanged;

	UFUNCTION(BlueprintNativeEvent)
	void SavePlayerState(USSaveGame* SaveObject);

	UFUNCTION(BlueprintNativeEvent)
	void LoadPlayerState(USSaveGame* SaveObject);
};
