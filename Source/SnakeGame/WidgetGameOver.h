// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SnakeGameMode.h"
#include "Blueprint/UserWidget.h"
#include "WidgetGameOver.generated.h"

class UButton;

UCLASS()
class SNAKEGAME_API UWidgetGameOver : public UUserWidget
{
	GENERATED_BODY()

	public:
	void Setup();

public:

	UPROPERTY(meta = (BindWidget))
	UButton* RestartButton;

	UPROPERTY(meta = (BindWidget))
	UButton* ExitButton;

	UFUNCTION()
	void RestartButtonClicked();

	UFUNCTION()
	void ExitButtonClicked();

	virtual void NativeConstruct() override;

	ASnakeGameMode* GetGameMode();

protected:

	UPROPERTY()
	APlayerController* PlayerController = nullptr;

};
