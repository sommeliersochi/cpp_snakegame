﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <CoreMinimal.h>
#include <GameFramework/Actor.h>

#include "Interactable.h"
#include "ADeadWall.generated.h"

UCLASS()
class SNAKEGAME_API AADeadWall : public AActor, public IInteractable
{
	GENERATED_BODY()

public:

	AADeadWall();

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

protected:
	
	UPROPERTY(EditDefaultsOnly, Category= "Component")
	UStaticMeshComponent* StaticMeshComp;
	
};
