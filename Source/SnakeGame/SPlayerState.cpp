// Fill out your copyright notice in the Description page of Project Settings.


#include "SPlayerState.h"

#include "SSaveGame.h"

int32 ASPlayerState::GetCresdits() const
{
	return PlayScore;
}

int32 ASPlayerState::GetBestResult() const
{
	return BestScore;
}

void ASPlayerState::AddScore(int32 Delta)
{
	PlayScore += Delta;

	OnCreditsChanged.Broadcast(this, PlayScore, Delta);
}

void ASPlayerState::LoadPlayerState_Implementation(USSaveGame* SaveObject)
{
	if(SaveObject)
	{
		BestScore = SaveObject->BestResult;
	}
}

void ASPlayerState::SavePlayerState_Implementation(USSaveGame* SaveObject)
{
	if(SaveObject)
	{
		if(PlayScore > BestScore)
		{
			SaveObject->BestResult = PlayScore;
		}
		else
		{
			SaveObject->BestResult = BestScore;
		}
	}
}

