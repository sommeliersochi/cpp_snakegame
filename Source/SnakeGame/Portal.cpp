﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Portal.h"

#include "MagicNumberTypes.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"

APortal::APortal()
{
	PrimaryActorTick.bCanEverTick = true;

	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>("MeshComp");
	RootComponent = StaticMeshComp;
	float Scale = 0.95f;
	StaticMeshComp->SetRelativeScale3D(FVector(Scale, Scale, Scale));
}

void APortal::BeginPlay()
{
	Super::BeginPlay();

	//SetActorRotation(UKismetMathLibrary::MakeRotFromX(FVector::ZeroVector - GetActorLocation()));
}

void APortal::Interact(AActor* Interactor, bool bIsHead)
{
	IInteractable::Interact(Interactor, bIsHead);

	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		if (const APlayerPawnBase* PlayerPawnBase = Interactor->GetInstigator()->GetController()->GetPawn<APlayerPawnBase>())
		{
			PlayerPawnBase->PlayPortalSound(GetActorLocation());
		}
		
		if (!Snake->InPortal)
		{
			Snake->InPortal = true;

			FVector MovementVector(FVector::ZeroVector);
			switch (Snake->LastMoveDirection)
			{
			case EMovementDirection::UP:
				MovementVector.X -= (GetPortalDistance() - ELEMENT_SIZE * 2);
				break;
			case EMovementDirection::DOWN:
				MovementVector.X += (GetPortalDistance() - ELEMENT_SIZE * 2);
				break;
			case EMovementDirection::LEFT:
				MovementVector.Y -= (GetPortalDistance() - ELEMENT_SIZE * 2);
				break;
			case EMovementDirection::RIGHT:
				MovementVector.Y += (GetPortalDistance() - ELEMENT_SIZE * 2);
				break;
			}
			Snake->GetSnakeElements()[0]->AddActorWorldOffset(MovementVector);
			Snake->InPortal = false;
		}
	}
}

void APortal::SetPortalDistance(float Value)
{
	PortalDistance = GetPortalDistance() * Value;
}

float APortal::GetPortalDistance()
{
	return PortalDistance;
}
